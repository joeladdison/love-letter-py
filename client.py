#!/usr/bin/env python

# 2310client (CSSE2310 Assignment 4, 2014)
#
# Copyright (c) 2014, Joel Addison (jea)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from __future__ import print_function
import sys
import os
import socket
import signal

from game import *


# Exit statuses
EXIT_SUCCESS = 0
EXIT_ARGS = 1
EXIT_PLAYER_NAME = 2
EXIT_GAME_NAME = 3
EXIT_PORT_NUM = 4
EXIT_CONN = 5
EXIT_GAME_INFO = 6
EXIT_MESSAGE = 7
EXIT_SERVER_LOSS = 8
EXIT_END_INPUT = 9
EXIT_SYSTEM = 20

EXIT_STATUS = {
    EXIT_SUCCESS: '',
    EXIT_ARGS: 'Usage: client name game_name port host',
    EXIT_PLAYER_NAME: 'Invalid player name',
    EXIT_GAME_NAME: 'Invalid game name',
    EXIT_PORT_NUM: 'Invalid server port',
    EXIT_CONN: 'Server connection failed',
    EXIT_GAME_INFO: 'Invalid game information received from server',
    EXIT_MESSAGE: 'Bad message from server',
    EXIT_SERVER_LOSS: 'Unexpected loss of server',
    EXIT_END_INPUT: 'End of player input',
    EXIT_SYSTEM: 'Unexpected system call failure',
}

DEBUG = False


def d(message):
    if DEBUG:
        print("[{0}] DEBUG".format(time.ctime()), message, file=sys.stderr)


class Game(object):

    def __init__(self):
        self.players = []
        self.me = None
        self.current = 0
        self.played = []


def exit(status):
    message = EXIT_STATUS[status]
    if message:
        print(message, file=sys.stderr)

    # Exit the client.
    sys.exit(status)


def connect_to_server(hostname, port):
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((hostname, port))
    except socket.error:
        exit(EXIT_CONN)
    return sock


def receive_message(sock_file, game_start=False):
    server_error = False
    try:
        data = sock_file.readline()
    except socket.error:
        server_error = True
    if (server_error or not data) and not game_start:
        # Unexpected loss of server
        exit(EXIT_SERVER_LOSS)
    elif not data.rstrip('\n') and not game_start:
        # Message is blank, which is invalid
        exit(EXIT_MESSAGE)
    elif (server_error or not data or not data.rstrip('\n')) and game_start:
        # Bad message during game setup information
        exit(EXIT_GAME_INFO)
    return data.rstrip('\n')


def user_input(prompt):
    try:
        data = raw_input(prompt)
    except EOFError:
        exit(EXIT_END_INPUT)
    return data


def print_status(game):
    for p in game.players:
        print("{0}({1}){2}:{3}".format(
            p.id, p.name, p.status, ''.join(p.cards)))
    print("You are holding:{0}".format(''.join(game.me.hand)))


def new_round(game, message):
    if len(message) != 10 or message[8] != ' ' or not valid_card(message[9]):
        exit(EXIT_MESSAGE)

    # Reset players and game
    for p in game.players:
        p.reset()
    game.played = []

    card = message[9]
    game.me.hand = [card, CARD_NONE]


def choose_card(game):
    card = CARD_NONE
    while True:
        card = user_input("card>")
        if valid_play(game.me, card):
            break

    target = PLAYER_NONE
    if card in NEEDS_TARGET:
        target = get_target(game, card)
        if target != PLAYER_NONE:
            # Get user to select a target
            while True:
                target = user_input("target>")
                if valid_target(game, target, card):
                    break

    guess = CARD_NONE
    if card == '1' and target != PLAYER_NONE:
        while True:
            guess = user_input("guess>")
            if valid_guess(card, target, guess):
                break

    return card, target, guess


def update_state(game, player_id, card):
    p = game.players[ord(player_id) - ord('A')]
    p.cards.append(card)
    game.played.append(card)
    if card == '4':
        p.status = STATUS_PROTECTED
    elif p.status == STATUS_PROTECTED:
        p.status = STATUS_NORMAL


def your_turn(game, message):
    if len(message) != 10 or message[8] != ' ' or not valid_card(message[9]):
        exit(EXIT_MESSAGE)

    # Add new card to player's hand
    game.me.hand[1] = message[9]

    if game.me.status == STATUS_PROTECTED:
        game.me.status = STATUS_NORMAL

    print_status(game)

    while True:
        card, target, guess = choose_card(game)
        game.me.send_message("{0}{1}{2}".format(card, target, guess))
        msg = receive_message(game.me.sock_file)
        d("FROM SERVER: {0}".format(msg))
        if msg == 'YES':
            break
        elif msg != 'NO':
            exit(EXIT_MESSAGE)

    # Update hand and discarded cards
    game.me.hand.remove(card)
    game.me.hand.append(CARD_NONE)
    update_state(game, game.me.id, card)


def move_happened(game, message):
    if (len(message) != 21 or message[12] != ' ' or message[17] != '/' or
            not valid_player(game, message[13]) or
            not valid_card(message[14]) or
            not valid_player(game, message[15]) or
            not valid_card(message[16]) or
            not valid_player(game, message[18]) or
            not valid_card(message[19]) or
            not valid_player(game, message[20])):
        exit(EXIT_MESSAGE)

    move = Move.from_string(message[13:])
    print(str(move))

    if game.me.id != move.player:
        update_state(game, move.player, move.card)

    if move.drop_player != PLAYER_NONE:
        if move.drop_card == PLAYER_NONE:
            exit(EXIT_MESSAGE)
        update_state(game, move.drop_player, move.drop_card)

    if move.eliminated != PLAYER_NONE:
        p = game.players[move.eliminated_idx]
        p.status = STATUS_OUT
        if p == game.me:
            p.hand = [CARD_NONE, CARD_NONE]


def replace(game, message):
    if len(message) != 9 or message[7] != ' ' or not valid_card(message[8]):
        exit_player(EXIT_MESSAGE)

    game.me.hand[0] = message[8]


def scores(game, message):
    if len(message) != (6 + 2 * len(game.players)):
        exit_player(EXIT_MESSAGE)

    for i, c in enumerate(message[6:]):
        if i % 2:
            # Score
            if '0' <= c <= '4':
                game.players[i // 2].score = int(c)
            else:
                exit(EXIT_MESSAGE)
        else:
            # Space
            if c != ' ':
                exit(EXIT_MESSAGE)

    message = "Scores:"
    for p in game.players:
        message += " {0}={1}".format(p.name, p.score)
    print(message)


def process_message(game, message):
    d("FROM SERVER: {0}".format(message))
    if message.startswith(MSG_NEW_ROUND):
        new_round(game, message)
    elif message.startswith(MSG_YOUR_TURN):
        your_turn(game, message)
    elif message.startswith(MSG_HAPPENED):
        move_happened(game, message)
    elif message.startswith(MSG_REPLACE):
        replace(game, message)
    elif message.startswith(MSG_SCORES):
        scores(game, message)
    elif message == MSG_GAME_OVER:
        print("Game over")
        exit(EXIT_SUCCESS)
    else:
        exit(EXIT_MESSAGE)


def play_game(game):
    while True:
        message = receive_message(game.me.sock_file)
        process_message(game, message)


def initialise_game(player_name, game_name, sock):
    sock_file = sock.makefile(bufsize=0)
    # Send player name
    send_message(player_name, sock_file)

    # Send game name
    send_message(game_name, sock_file)

    # Get number of players and player ID
    details = receive_message(sock_file, game_start=True)
    parts = details.split(' ')
    try:
        assert len(parts) == 2
        num_players = int(parts[0])
        assert 2 <= num_players <= 4
        my_id = parts[1]
        assert my_id != '-' and 0 <= (ord(my_id) - ord('A')) < num_players
    except:
        exit(EXIT_GAME_INFO)

    # Setup players in the game
    game = Game()
    for i in range(num_players):
        name = receive_message(sock_file, game_start=True)
        p = Player(name)
        p.id = chr(ord('A') + i)
        game.players.append(p)
        if p.id == my_id:
            # We are this player in the game
            game.me = p
            game.current = i
            p.sock = sock
            p.sock_file = sock_file

    return game


def signal_handler(signal, frame):
    sys.exit(0)


def main():
    signal.signal(signal.SIGINT, signal_handler)

    if not 4 <= len(sys.argv) <= 5:
        exit(EXIT_ARGS)

    if os.getenv('DEBUG'):
        global DEBUG
        DEBUG = True

    player_name = sys.argv[1]
    if not player_name or '\n' in player_name:
        exit(EXIT_PLAYER_NAME)

    game_name = sys.argv[2]
    if not game_name or '\n' in game_name:
        exit(EXIT_GAME_NAME)

    port = validate_port(sys.argv[3])
    if not port:
        exit(EXIT_PORT_NUM)

    hostname = 'localhost'
    if len(sys.argv) == 5:
        hostname = sys.argv[4]

    sock = connect_to_server(hostname, port)
    game = initialise_game(player_name, game_name, sock)
    play_game(game)

    sock.close()

    sys.exit(0)


if __name__ == '__main__':
    main()
