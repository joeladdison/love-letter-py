# General Game functions (CSSE2310 Assignment 4, 2014)
#
# Copyright (c) 2014, Joel Addison (jea)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from __future__ import print_function
import socket

# Hub message types
MSG_NEW_ROUND = "newround"
MSG_YOUR_TURN = "yourturn"
MSG_HAPPENED = "thishappened"
MSG_REPLACE = "replace"
MSG_SCORES = "scores"
MSG_GAME_OVER = "gameover"

# No player
PLAYER_NONE = '-'

# No card
CARD_NONE = '-'

# Player status
STATUS_OUT = '-'
STATUS_PROTECTED = '*'
STATUS_NORMAL = ' '

MIN_PLAYERS = 2
MAX_PLAYERS = 4

# Other constants
MAX_INPUT = 79
NUM_CARDS = 16

# Card information
NEEDS_TARGET = "1356"

# The standard number of each type of card.
CARD_COUNTS = [5, 2, 2, 2, 2, 1, 1, 1]


class Player(object):

    def __init__(self, name, sock=None, sock_file=None):
        self.id = ''
        self.name = name
        self.status = STATUS_NORMAL
        self.cards = []
        self.hand = []
        self.score = 0
        self.sock = sock
        self.sock_file = sock_file

    def reset(self):
        self.status = STATUS_NORMAL
        self.cards = []
        self.hand = []

    def send_message(self, message, **kwargs):
        send_message(message, self.sock_file, **kwargs)


class Move(object):

    def __init__(self, player, card, target, guess):
        self.player = player
        self.card = card
        self.target = target
        self.guess = guess
        self.drop_player = '-'
        self.drop_card = '-'
        self.eliminated = '-'

    @classmethod
    def from_string(cls, move):
        s = cls(move[0], move[1], move[2], move[3])
        s.drop_player = move[5]
        s.drop_card = move[6]
        s.eliminated = move[7]
        return s

    def __repr__(self):
        return "{0}{1}{2}{3}/{4}{5}{6}".format(
            self.player, self.card, self.target, self.guess,
            self.drop_player, self.drop_card, self.eliminated)

    @property
    def player_idx(self):
        return ord(self.player) - ord('A')

    @property
    def target_idx(self):
        return ord(self.target) - ord('A')

    @property
    def drop_player_idx(self):
        return ord(self.drop_player) - ord('A')

    @property
    def eliminated_idx(self):
        return ord(self.eliminated) - ord('A')

    def __str__(self):
        message = "Player {self.player} discarded {self.card}"
        if self.target != PLAYER_NONE:
            message += " aimed at {self.target}"
            if self.guess != CARD_NONE:
                message += " guessing {self.guess}"
            if self.drop_player != PLAYER_NONE:
                message += ". This forced {self.drop_player} "
                message += "to discard {self.drop_card}"
        if self.eliminated != PLAYER_NONE:
            message += ". {self.eliminated} was out"
        message += "."
        return message.format(self=self)


def valid_player(game, id):
    if not game or not id or len(id) != 1 or (id != '-' and (
            ord(id) < ord('A') or (ord(id) - ord('A')) >= len(game.players))):
        return False
    return True


def valid_card(card):
    if not card or len(card) != 1 or (
            card != '-' and (ord(card) < ord('0') or ord(card) > ord('8'))):
        return False
    return True


def valid_play(player, card):
    if not valid_card(card):
        return False

    if '7' in player.hand and ('5' in player.hand or '6' in player.hand):
        if card == '7':
            return True
    elif card in player.hand:
        return True

    return False


def valid_target(game, target_id, card):
    if not valid_player(game, target_id) or not valid_card(card):
        return False
    elif target_id == '-':
        if card not in NEEDS_TARGET or get_target(game, card) == '-':
            return True
    elif target_id == chr(game.current + ord('A')):
        if card == '5':
            return True
    elif game.players[ord(target_id) - ord('A')].status == STATUS_NORMAL:
        return True
    return False


def valid_guess(played, target_id, guess):
    if not valid_card(played) or not valid_card(guess):
        return False
    if played == '1':
        if target_id == '-' and guess == '-':
            return True
        elif target_id != '-' and ord('2') <= ord(guess) <= ord('8'):
            return True
    elif guess == '-':
        return True
    return False


def get_target(game, card):
    i = (game.current + 1) % len(game.players)

    while i != game.current:
        if game.players[i].status == STATUS_NORMAL:
            return game.players[i].id
        i = (i + 1) % len(game.players)

    if card == '5':
        # Self target
        return chr(game.current + ord('A'))

    return PLAYER_NONE


def validate_port(port):
    if not port.isdigit():
        return
    try:
        port = int(port)
    except ValueError:
        return
    if 1 <= port <= 65535:
        return port


def send_message(message, file, **kwargs):
    try:
        print(message, file=file, **kwargs)
        file.flush()
    except (socket.error, AttributeError):
        # We do not care about Broken Pipes at this stage
        pass
