Love Letter
===========

Implementation of the 2014 CSSE2310 Assignment 4 in Python.
Game is inspired by [Love Letter](http://www.alderac.com/loveletter/love-letter-tempest-edition/ "Love Letter"),
with a few simplifications.

## Getting a copy

After cloning the repository, you are able to setup the programs very simply:

    make

## Using the server

    ./2310serv adminport [[port deck]...]

The arguments for the server are:

 - `adminport` is the port number to use for administration commands
 - Pairs of arguments specifying different games servers to run:
  - `port`: A port to listen on
  - `deck`: The deck file to use for games on that port

For additional functionality and debug information, set the DEBUG environment
variable before running the server.

    DEBUG=True ./2310serv adminport [[port deck]...]

### Administration

The administration section of the server allows two commands. It allows a single
connection at a time. No prompt is printed, and any invalid commands are silently
ignored.

Commands:

 - `P`: Start a game server on an additional port. (eg. `P4001 ex.decks`)
 - `S` (no arguments): Sends a table of statistics (sorted by player name)
  - Player name
  - Number of games played (including incomplete games)
  - Number of rounds won
  - Number of games won (including ties)

If a command is successful, `OK` is returned. If an error occurred, this will
be sent back to the user.

An example of communication with the administration server:

    $ nc localhost 4000
    S
    Alfonz,2,1,0
    Harry,3,5,1
    Willem,3,9,2
    OK
    P4002 ex.decks
    OK
    P4002 ex.decks
    Invalid portnumber

## Using the client

    ./2310client name game_name host port

The arguments for the client are:

 - `name` is your player's name in the game.
 - `game_name` is the name of the game you want to join. This also determines
 the number of players in a game. A game name starting with:
  - '2': a two player game
  - '3': a three player game
  - anything else: a four player game
 - `host` is the address of the machine the server is running on (eg. `localhost`)
 - `port` is the port the server is running on
