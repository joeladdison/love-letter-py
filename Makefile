.DEFAULT: all
.PHONY: all clean

TARGETS = 2310client 2310serv

all: $(TARGETS)

2310client:
	chmod u+x client.py
	ln -s client.py 2310client

2310serv:
	chmod u+x server.py
	ln -s server.py 2310serv

clean:
	rm -f $(TARGETS)
	rm -f *.pyc
