#!/usr/bin/env python

# serv499 (CSSE2310 Assignment 4, 2013)
#
# Copyright (c) 2014, Joel Addison (jea)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from __future__ import print_function, absolute_import
import sys
import os
import socket
import signal
import threading
import errno
import time
import select
import datetime
import pickle

from game import *

# Debug mode
DEBUG = False

# Allow creation of new game servers to be disabled
ALLOW_ADMIN_PORT = True

BACKLOG = 5
HOSTNAME = ''
PENDING_TIMEOUT = datetime.timedelta(minutes=10)
MAX_INPUT = 64 * 1024
INPUT_TIMEOUT = 30
SCORES_FILE = 'scores.pkl'

# Exit statuses
EXIT_SUCCESS = 0
EXIT_ARGS = 1
EXIT_DECK_ACCESS = 2
EXIT_DECK_CONTENTS = 3
EXIT_PORT_NUM = 4
EXIT_PORT_LISTEN = 5
EXIT_SYSTEM = 9

EXIT_STATUS = {
    EXIT_SUCCESS: '',
    EXIT_ARGS: 'Usage: 2310serv adminport [[port deck]...]',
    EXIT_DECK_ACCESS: 'Unable to access deckfile',
    EXIT_DECK_CONTENTS: 'Error reading deck',
    EXIT_PORT_NUM: 'Invalid port number',
    EXIT_PORT_LISTEN: 'Unable to listen on port',
    EXIT_SYSTEM: 'System error',
}

# Global variables, for use in signal handler.
admin = None
servers = []
scores = {}
server_ports = []
# Lock for accessing the scores
scores_lock = threading.Lock()


def d(message):
    if DEBUG:
        print("[{0}] DEBUG".format(time.ctime()), message, file=sys.stderr)


class Deck:
    def __init__(self, cards):
        self.cards = cards
        self.next = None


class Score(object):

    def __init__(self, name):
        self.name = name
        self.games = 0
        self.rounds_won = 0
        self.games_won = 0

    def played_game(self):
        self.games += 1

    def won_round(self, count=1):
        self.rounds_won += count

    def won_game(self):
        self.games_won += 1

    def __str__(self):
        return "{0},{1},{2},{3}".format(
            self.name, self.games, self.rounds_won, self.games_won)


class Server(object):

    def __init__(self, port, decks, sock):
        self.sock = sock
        self.port = port
        self.decks = decks
        self.pending = {}
        self.pending_games = []
        self.games = []
        self.threads = []
        self.running = True

    def d(self, message):
        d("(SERVER {0}) {1}".format(self.port, message))

    def run(self):
        self.d("Running server")
        while self.running:
            # Accept connections
            try:
                client, address = self.sock.accept()
            except socket.error as e:
                if e.errno == errno.EMFILE and self.pending_games:
                    # Clean up oldest pending game
                    d("Removing pending game due to hitting file limit")
                    name = self.pending_games.pop()
                    game = self.pending[name]
                    for p in game.players:
                        close_connection(p.sock, p.sock_file)
                    del self.pending[name]
                    continue

            self.d("Accepted connection {0}".format(address))
            self.accept_connection(client)

            # Clean up old pending games
            now = datetime.datetime.now()
            for game_name in self.pending_games[:]:
                g = self.pending[game_name]
                if g.start_time + PENDING_TIMEOUT < now:
                    self.d("Removing pending game '{0}' due to timeout".format(
                        g.name))
                    self.pending_games.remove(game_name)
                    del self.pending[game_name]

            self.cleanup_threads()

    def accept_connection(self, client):
        sock_file = client.makefile(bufsize=0)

        # Get player name
        player_name = get_client_input_timeout(sock_file)
        if not player_name:
            close_connection(client, sock_file)
            return

        # Get game name
        game_name = get_client_input_timeout(sock_file)
        if not game_name:
            close_connection(client, sock_file)
            return

        # Create the player
        p = Player(player_name, client, sock_file)

        # Add player to pending game
        game = self.pending.setdefault(game_name, Game())
        if game_name not in self.pending_games:
            self.pending_games.append(game_name)
            game.name = game_name
            game.server = self
            if game_name[0] == '2':
                game.player_count = 2
            elif game_name[0] == '3':
                game.player_count = 3
        game.players.append(p)
        game.start_time = datetime.datetime.now()
        self.d("Connection: Player: {0!r}, Game: {1!r}".format(
            p.name, game_name))

        # Check if we need to start the game
        if len(game.players) == game.player_count:
            game.initialise()
            game.deck = self.decks
            # Add game to list of running games
            self.games.append(game)
            # Remove from pending
            del self.pending[game.name]
            self.pending_games.remove(game.name)

            # Start thread for game
            self.d("Starting game: {0!r}".format(game.name))
            gt = GameThread(game)
            self.threads.append(gt)
            gt.start()

    def cleanup_threads(self):
        # Clean up threads
        for t in self.threads:
            t.join(0)
            if not t.is_alive():
                self.threads.remove(t)

    def shutdown(self):
        self.running = False
        cancel_accept(self.port)
        # self.sock.shutdown(socket.SHUT_WR)
        self.sock.close()

        for g in self.games:
            g.end_game()
        self.cleanup_threads()


class ServerThread(threading.Thread):

    def __init__(self, server):
        super(ServerThread, self).__init__()
        self.server = server

    def run(self):
        self.server.run()


class Game(object):

    def __init__(self):
        self.name = None
        self.server = None
        self.player_count = 4
        self.current = 0
        self.players = []
        self.deck = None
        self.deck_pos = 0
        self.running = True
        self.start_time = None

    def initialise(self):
        self.players = sorted(self.players, key=lambda p: p.name)

        # Assign IDs and send each player their ID and player count
        for i, p in enumerate(self.players):
            p.id = chr(ord('A') + i)
            p.send_message("{0} {1}".format(self.player_count, p.id))

        # Send player names
        for p in self.players:
            self.send_message_all(p.name)

    def end_game(self):
        self.running = False
        # Send game over message
        self.send_message_all(MSG_GAME_OVER)
        # Close the connections
        for p in self.players:
            close_connection(p.sock, p.sock_file)

    def get_card(self):
        # Initially choose discarded card
        card = self.deck.cards[0]

        # Replace card if there are cards left in the deck
        if self.deck_pos < len(self.deck.cards):
            card = self.deck.cards[self.deck_pos]
            self.deck_pos += 1

        return card

    def send_message_all(self, message, **kwargs):
        for p in self.players:
            p.send_message(message, **kwargs)

    def play(self):
        overall_winner = False
        while not overall_winner:
            # Discard the first card
            self.deck_pos = 1

            self.new_round()

            winner = False
            i = 0
            while not winner:
                p = self.players[i]

                if p.status != STATUS_OUT:
                    self.current = i
                    self.play_turn(p)
                    if not self.running:
                        break

                    winner = self.round_winner()

                i += 1
                if i >= len(self.players):
                    i = 0

            if not self.running:
                # Game finished early
                break

            overall_winner = self.game_winner()

            # Move to the next deck
            self.deck = self.deck.next

        self.update_scores()
        self.end_game()

    def new_round(self):
        for p in self.players:
            p.reset()
            card = self.get_card()
            p.hand.append(card)
            p.send_message("{0} {1}".format(MSG_NEW_ROUND, card))

    def play_turn(self, player):
        # Reset status to normal, as protection finishes at start of turn
        if player.status == STATUS_PROTECTED:
            player.status = STATUS_NORMAL

        card = self.get_card()
        player.hand.append(card)
        player.send_message("{0} {1}".format(MSG_YOUR_TURN, card))

        while True:
            move = get_client_input_timeout(player.sock_file)

            if move is None:
                self.running = False
                return

            if (len(move) != 3 or not valid_play(player, move[0]) or
                    not valid_target(self, move[1], move[0]) or
                    not valid_guess(*move)):
                # Move was not valid, so ask again
                player.send_message("NO")
            else:
                # Move is valid
                player.send_message("YES")
                break

        played = move[0]
        target = move[1]
        guess = move[2]

        # Update player's hand
        player.hand.remove(played)
        player.cards.append(played)

        m = Move(player.id, played, target, guess)
        self.process_move(player, m)

        self.send_message_all("{0} {1!r}".format(MSG_HAPPENED, m))

    def process_move(self, player, move):
        # TODO: Check a player can target a player
        drop = None
        eliminated = None
        replacement = ''
        if move.target != PLAYER_NONE:
            target = self.players[move.target_idx]
            if move.card == '1':
                # Guess another player
                if move.guess in target.hand:
                    drop = eliminated = target
            elif move.card == '3':
                # Compare hands
                if player.hand[0] < target.hand[0]:
                    drop = eliminated = player
                elif target.hand[0] < player.hand[0]:
                    drop = eliminated = target
            elif move.card == '5':
                # Force player to discard hand
                drop = target
                if target.hand[0] == '8':
                    eliminated = target
                else:
                    if target.hand[0] == '4':
                        target.status = STATUS_PROTECTED
                    replacement = self.get_card()
            elif move.card == '6':
                # Players swap their hands
                player.hand[0], target.hand[0] = target.hand[0], player.hand[0]
                for p in (player, target):
                    p.send_message("{0} {1}".format(MSG_REPLACE, p.hand[0]))

        if move.card == '8':
            # Player is out
            eliminated = player
        elif move.card == '4':
            player.status = STATUS_PROTECTED

        if drop:
            move.drop_card = drop.hand[0]
            move.drop_player = drop.id
            drop.cards.append(drop.hand[0])
            drop.hand = []
            if replacement:
                drop.hand.append(replacement)
                drop.send_message("{0} {1}".format(MSG_REPLACE, replacement))

        if eliminated:
            move.eliminated = eliminated.id
            eliminated.status = STATUS_OUT

    def round_winner(self):
        # Count alive players and find current leaders
        alive = len(self.players)
        winners = []
        highest = '0'
        for p in self.players:
            if p.status == STATUS_OUT:
                alive -= 1
            elif p.hand[0] > highest:
                highest = p.hand[0]
                winners = [p]
            elif p.hand[0] == highest:
                winners.append(p)

        if self.deck_pos == NUM_CARDS or alive == 1:
            # We have a winner for this round
            for p in winners:
                p.score += 1

            # Send scores message to players
            game_scores = ' '.join(str(p.score) for p in self.players)
            self.send_message_all("{0} {1}".format(MSG_SCORES, game_scores))
            return True
        return False

    def game_winner(self):
        game_scores = [p.score for p in self.players]
        if 4 in game_scores:
            # There is an overall winner
            return True
        return False

    def update_scores(self):
        with scores_lock:
            global scores
            for p in self.players:
                s = scores.setdefault(p.name, Score(p.name))
                s.played_game()
                if p.score:
                    s.won_round(p.score)
                if p.score == 4:
                    s.won_game()


class GameThread(threading.Thread):
    def __init__(self, game):
        super(GameThread, self).__init__()
        self.game = game

    def run(self):
        self.game.play()


class AdminServer(object):
    def __init__(self, port, sock):
        self.port = port
        self.sock = sock
        self.running = True

        self.d("Server started")

    def d(self, message):
        d("(ADMIN {0}) {1}".format(self.port, message))

    def run(self):
        while self.running:
            # Accept connection
            try:
                client, address = self.sock.accept()
            except socket.error:
                continue

            self.d("Accepted connection {0}".format(address))

            sock_file = client.makefile(bufsize=0)

            while self.running:
                message = get_client_input_timeout(sock_file)
                if message is None:
                    close_connection(client, sock_file)
                    break
                if not message:
                    continue
                elif message[0] == 'P':
                    # Start a new server
                    self.new_server(message[1:], sock_file)
                elif message == 'S':
                    # Send statistics
                    self.send_statistics(sock_file)
                elif DEBUG and message.startswith('admin '):
                    message = message[6:]
                    if message.startswith('save'):
                        self.save_scores(message, sock_file)
                    elif message.startswith('load'):
                        self.load_scores(message, sock_file)
                    elif message.startswith('timeout'):
                        self.adjust_timeout(message, sock_file)

    def new_server(self, message, sock_file):
        details = message.split(' ')
        if len(details) == 2:
            if not ALLOW_ADMIN_PORT:
                send_message(
                    'Port creation disabled to prevent abuse.', sock_file)
            elif create_server(details[0], details[1], sock_file):
                send_message('OK', sock_file)

    def send_statistics(self, sock_file):
        with scores_lock:
            for name in sorted(scores):
                send_message(str(scores[name]), sock_file)
        send_message('OK', sock_file)

    def save_scores(self, message, sock_file):
        parts = message.split()
        path = SCORES_FILE
        if len(parts) > 2 and parts[1]:
            path = parts[1]
        if save_scores(path):
            send_message('OK', sock_file)
            self.d("Saved scores to {0}".format(path))

    def load_scores(self, message, sock_file):
        parts = message.split()
        path = SCORES_FILE
        if len(parts) > 2 and parts[1]:
            path = parts[1]
        if load_scores(path):
            send_message('OK', sock_file)
            self.d("Loaded scores from {0}".format(path))

    def adjust_timeout(self, message, sock_file):
        parts = message.split()
        if len(parts) >= 2:
            # Attempt to set a new timeout
            try:
                limit = int(parts[1])
            except ValueError:
                return
            global INPUT_TIMEOUT
            INPUT_TIMEOUT = limit
            self.d("New timeout {0}".format(INPUT_TIMEOUT))
        else:
            # Send current timeout
            send_message(
                "Current timeout is {0}".format(INPUT_TIMEOUT), sock_file)
        send_message('OK', sock_file)

    def shutdown(self):
        self.running = False
        cancel_accept(self.port)
        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()


def exit(status, output=None):
    message = EXIT_STATUS[status]
    if message:
        stream = output
        if output is None:
            stream = sys.stderr
        else:
            # Print extra debug information.
            d(message)

        send_message(message, stream)

    if output is None:
        # Exit the server.
        cleanup()
        sys.exit(status)


def cancel_accept(port):
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(('localhost', port))
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
    except socket.error:
        pass


def get_client_input_timeout(socket_file, timeout=INPUT_TIMEOUT):
    client_error = False
    memory_error = False
    data = ''
    try:
        rlist, _, _ = select.select([socket_file], [], [], timeout)
        if rlist:
            data = socket_file.readline(MAX_INPUT)
        else:
            # Timeout
            client_error = True
            send_message("MSorry, too slow.", socket_file)
            d("Kicked connection due to read timeout.")
    except socket.error:
        client_error = True
    except MemoryError:
        memory_error = True

    if memory_error or len(data) >= MAX_INPUT:
        # Message received is too large.
        send_message("MNo thanks, I think that's too big", socket_file)
        d("Kicked connection due to memory use.")
        return None
    elif client_error or not data:
        # Client has disconnected unexpectedly.
        d("Client exited unexpectedly.")
        return None
    d("Message received {0!r}".format(data))
    return data.rstrip('\n')


def close_connection(sock, sock_file):
    try:
        # Shutdown the socket
        if sock:
            sock.shutdown(socket.SHUT_RDWR)

        # Close the socket file
        if sock_file:
            sock_file.close()

        # Close the socket
        if sock:
            sock.close()
    except socket.error:
        # Don't care, we were closing connection anyway
        pass


def read_decks(path, output=None):
    """Read in multiple decks from a deck file.
    Returns a linked list of Decks.
    """
    try:
        head = None
        tail = None
        with open(path, "r") as f:
            line = f.readline()
            while line:
                count = [0, 0, 0, 0, 0, 0, 0, 0]
                if len(line) != 17:
                    exit(EXIT_DECK_CONTENTS, output)
                    break
                line = line.rstrip('\n')
                for c in line:
                    if not valid_card(c) or c == '-':
                        exit(EXIT_DECK_CONTENTS, output)
                    count[int(c) - 1] += 1
                if count != CARD_COUNTS:
                    exit(EXIT_DECK_CONTENTS, output)
                    break
                deck = Deck(list(line))
                if head is None:
                    head = tail = deck
                else:
                    tail.next = deck
                    tail = tail.next
                line = f.readline()
            if not head:
                exit(EXIT_DECK_CONTENTS, output)
            tail.next = head
            return head
    except IOError:
        exit(EXIT_DECK_ACCESS, output)


def create_server(port, deck, output=None):
    # Read in decks for this server
    decks = read_decks(deck, output)
    if not decks:
        return False

    # Attempt to start the server
    port = validate_port(port)
    if not port or port in server_ports:
        exit(EXIT_PORT_NUM, output)
        return False

    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((HOSTNAME, port))
        sock.listen(BACKLOG)
    except socket.error:
        if sock:
            sock.close()
        exit(EXIT_PORT_LISTEN, output)
        return False

    # Record port as being used
    server_ports.append(port)

    # Start a thread to run the server
    s = Server(port, decks, sock)
    d("Started server {0} with deck {1!r}".format(port, deck))
    st = ServerThread(s)
    servers.append(st)
    st.start()
    return True


def admin_server(port):
    # Attempt to start the server
    port = validate_port(port)
    if not port or port in server_ports:
        exit(EXIT_PORT_NUM)

    # Attempt to start the server
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((HOSTNAME, port))
        sock.listen(BACKLOG)
    except socket.error:
        if sock:
            sock.close()
        exit(EXIT_PORT_LISTEN)

    # Record port as being used
    server_ports.append(port)

    # Start admin server
    global admin
    admin = AdminServer(port, sock)
    admin.run()


def save_scores(path=SCORES_FILE):
    try:
        with open(path, 'w') as f:
            with scores_lock:
                pickle.dump(scores, f)
    except IOError:
        return False
    return True


def load_scores(path=SCORES_FILE):
    try:
        with open(path, 'r') as f:
            with scores_lock:
                global scores
                scores = pickle.load(f)
    except IOError:
        return False
    return True


def cleanup():
    if servers:
        # End running servers
        d("Killing Game servers")
        for st in servers:
            if st.is_alive():
                st.server.shutdown()
                st.join(0)

    if admin:
        d("Killing Admin server")
        # End the admin server
        admin.shutdown()

    if DEBUG:
        d("Saving scores")
        save_scores()


def signal_handler(signal, frame):
    d("SIGINT received")
    cleanup()
    # Exit the program
    sys.exit(EXIT_SUCCESS)


def main():
    signal.signal(signal.SIGINT, signal_handler)

    if len(sys.argv) % 2 != 0:
        exit(EXIT_ARGS)

    if os.getenv('DEBUG'):
        # Enable debug mode, and load scores if available.
        global DEBUG
        DEBUG = True
        load_scores()

    if os.getenv('ALLOW_ADMIN_PORT', '') == 'False':
        # Disable creation of new game servers from Admin interface
        global ALLOW_ADMIN_PORT
        d("Do not allow P within admin server.")
        ALLOW_ADMIN_PORT = False

    # Start game servers
    for i in range(2, len(sys.argv), 2):
        port = sys.argv[i]
        deck_path = sys.argv[i + 1]
        create_server(port, deck_path)

    # Start admin server
    admin_server(sys.argv[1])


if __name__ == '__main__':
    main()
